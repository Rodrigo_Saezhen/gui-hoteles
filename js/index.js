<script>
       
         $(function () {
             $('[data-toggle="tooltip"]').tooltip();
             $('[data-toggle="popover"]').popover();
             $('.carousel').carousel({
                 interval: 3000
             });
            
            
             $('#contactobtn').on('show.bs.modal',function(e){
                 console.log('el modal se esta mostrando'); 
                 $('#contactobtn').removeClass('btn-outline-success');
                 $('#contactobtn').addClass('btn-primary');  
                 $('#contactobtn').prop('disabled',true);
             });
             $('#contactobtn').on('shown.bs.modal',function(e){
                 console.log('el modal registrar se mostró'); 
                 
             });
             $('#contactobtn').on('hide.bs.modal',function(e){
                 console.log('el modal registrar se oculta'); 
                 
             });
             $('#contactobtn').on('hidden.bs.modal',function(e){
                 console.log('el modal registar se oculto'); 
                 
             });
         });
    </script>